import p from '../pages/examplePage'
import allureReporter from '@wdio/allure-reporter'
import D from '../utils/dataProvider'
import DB from '../utils/DB-utils'

describe('First Test Suite', () => {
    let user_id;

    after(() => {
        DB.deleteUserRecord(user_id)
    })

    D.parseCsvFile('test/resources/list.csv','utf-8').forEach((input, i) => {

        let testName = `${i+1}. login with username: ${input.username} && password: ${input.password}`;
        testName += `, should show the following error: ${input.expectedError}.`;

        it(testName, () => {
            allureReporter.addSeverity('critical')
            allureReporter.addDescription(`when loging with following cred username: ${input.username}, password: ${input.password}, user should get the following error: ${input.expectedError}`);
            allureReporter.addStep('fill user name and password and hit login button')
            p.loginWith(input.username, input.password)
                .waitForErrorMsg()
            expect(p.errorMsg).toHaveText(input.expectedError)
        });
    });

    it('01. Should not be able to login with invalid cred', () => {
        allureReporter.addSeverity('critical')
        allureReporter.addDescription('when credentials are invalid, error msg should pop up Invalid credentials specified');
        allureReporter.addStep('fill user name and password and hit login button')
        p.loginWith('spaceFrame', 'anything')
            .waitForErrorMsg()
        expect(p.errorMsg).toHaveText('Invalid credentials specified')
    });

    // it('02. should not be able to login without username', () => {
    //     allureReporter.addSeverity('low')
    //     p.loginWith('', 'anything')
    //         .waitForErrorMsg()
    //     expect(p.errorMsg).toHaveText('Please specify a username');
    // });

    // it('03. Should not be able to login without password', () => {
    //     p.loginWith('spaceFrame', '')
    //         .waitForErrorMsg()
    //     expect(p.errorMsg).toHaveText('Please specify a password');
    // });

    // it('04. Should not be able to login without credentials', () => {
    //     p.loginWith('', '')
    //         .waitForErrorMsg()
    //     expect(p.errorMsg).toHaveText('No username or password specified');
    // });

    it('1. DB integration example',()=>{
        // DB.submitDBQuery('SDS')
       const op = browser.submitDBQuery('select * from spaceframe_white.sb__user where id=1')
       console.log(op)
       const username = op[0].username
       p.loginWith(username, 'anything')
       browser.pause(3000)
    })

    it.only('Add a new user and login with',()=>{
        user_id = DB.insertNewUser('QATeam','qaSession@blue.com').insertId
        DB.addPass(user_id)
        p.loginWith('QATeam', 'spac3##')
        browser.pause(4000)
    })

});

