import P from '../pages/dashBoardPage'
import DB from '../utils/DB-utils'
import API from '../utils/API-utils'
let loginCookie
describe('Dashboard Test suite', () => {

    before(() => {
        loginCookie = API.getLoginCookies('spaceframe', 'spac3##')
        console.log(loginCookie)
        browser.setCookies({ name: 'PHPSESSID', value: loginCookie })
        browser.url('http://white.sf.bluesky.sh/_admin')
        // browser.loginWith('spaceframe', 'spac3##')
    })

    it('Should be able to navigate to cms dashboard', async () => {
        P.clickOnCmsDashboard()
        expect(P.cmsPageHeader).toBeDisplayed()
    })

})