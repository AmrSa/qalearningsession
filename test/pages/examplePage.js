
class ExamplePage {

    get username() {return $('#username')}
    get password() {return $('//input[@name="password"]')}
    get loginButton() {return $('#submit_login_form')}
    get errorMsg() {return $('#login_error')}
    get () {return }
    get () {return }
    get () {return }


    loginWith(username,password){
        this.username.waitForDisplayed()
        this.username.setValue(username)
        this.password.setValue(password)
        this.loginButton.click()
        return this;
    }

    waitForErrorMsg(){
        this.errorMsg.waitForDisplayed()
        return this;
    }

}

export default new ExamplePage();