
class DashBoard {

    get username() { return $('#username') }
    get password() { return $('//input[@name="password"]') }
    get loginButton() { return $('#submit_login_form') }
    get adminHeader() {return $('h2=Admin dashboard')}
    get cmsDashboard() { return $('div=CMS Dashboard')}  
    get cmsPageHeader() { return $('h2=CMS dashboard')}  
    get x() { return $('')}
    get x() { return $('')}
    get x() { return $('')}
    get x() { return $('')}
    get x() { return $('')}
    get x() { return $('')}
    get x() { return $('')}


    loginWith(username, password) {
        this.username.waitForDisplayed()
        this.username.setValue(username)
        this.password.setValue(password)
        this.loginButton.click()
        this.adminHeader.waitForDisplayed()
        return this;
    }

    clickOnCmsDashboard(){
        this.cmsDashboard.click()
        this.cmsPageHeader.waitForDisplayed()
        return this;
    }



}

export default new DashBoard();