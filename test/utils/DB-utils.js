import D from '../utils/dataProvider';
import mysql from 'mysql2'
import { Client } from 'ssh2';

let pool;

class DB {

     sshClient = new Client();
     dbServer = D.dbServer
     tunnelConfig = D.tunnelConfig
     forwardConfig = D.forwardConfig

    async main() {
        new Promise((resolve, reject) => {
            this.sshClient.on('ready', () => {
                this.sshClient.forwardOut(
                    this.forwardConfig.srcHost,
                    this.forwardConfig.srcPort,
                    this.forwardConfig.dstHost,
                    this.forwardConfig.dstPort,
                    (err, stream) => {
                        if (err) reject(err);
                        // create a new DB server object including stream
                        const updatedDbServer = {
                            ...this.dbServer,
                            stream
                        };            // connect to mysql
                        pool = mysql.createPool(updatedDbServer);
                    });
            }).connect(this.tunnelConfig);
            console.log('====> Connected to spaceFrame DB <====');
        });
    }

    submitDBQuery(query){
        return browser.call(async ()=> {
            const promisePool = pool.promise();
            const output = await promisePool.query(query)
            const results = JSON.parse(JSON.stringify(output))
            return await results[0];
        })
    }
    
    insertNewUser(username,email){
        return browser.call(async ()=>{
            const promisePool = pool.promise();
            const output = await promisePool
            .query(`INSERT INTO spaceframe_white.sb__user (username, email, title, first_name, last_name, is_active, is_admin)
            VALUES ('${username}','${email}','Mr','amr','salem',1,1);`)
            const results = JSON.parse(JSON.stringify(output))
            return await results[0];
        })
    }

    addPass(user_id){
        return browser.call(async ()=>{
            const promisePool = pool.promise();
            const output = await promisePool
            .query(`INSERT INTO spaceframe_white.sb__user_password (user_id, password, is_current) 
            VALUES (${user_id},'$2y$10$.AKVO5LvLiqoM64vfQC.d.U05DU3tXiiGlr0FULJTVVfzlWYsll4G',1);`)
            const results = JSON.parse(JSON.stringify(output))
            return await results;
        })
    }

    deleteUserRecord (user_id) {
        return browser.call(async()=>{
            const promisePool = pool.promise();
            const output = await promisePool
            .query(`DELETE FROM spaceframe_white.sb__user_password WHERE user_id=${user_id};
            DELETE FROM spaceframe_white.sb__user WHERE id=${user_id};`);
            const results = JSON.parse(JSON.stringify(output));
            return await results;
        });
    }
   

}

export default new DB();
