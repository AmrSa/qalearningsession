const fs = require('fs')

const D = exports;

D.inputs = [
    {
        username: 'spaceFrame',
        password: 'anything',
        expectedError: 'Invalid credentials specified'
    },
    {
        username: '',
        password: 'anything',
        expectedError: 'Please specify a username'
    },
    {
        username: 'spaceFrame',
        password: '',
        expectedError: 'Please specify a password'
    },
    {
        username: '',
        password: '',
        expectedError: 'No username or password specified'
    }
]

D.parseCsvFile = function(path,encoding) {
    const csvFile = fs.readFileSync(path,encoding)
    const parseCsv = csv => {
    const lines = csv.split('\n')
    const header = lines.shift().split(',')
    return lines.map(line =>{
        const bits = line.split(',');
        const obj = {};
        header.forEach((h,i) => obj[h] = bits[i])
        console.log(obj)
        return obj;
    })
}
return parseCsv(csvFile);
}


D.dbServer = {
    host: 'localhost',
    port: 3306,
    user: 'spaceframe_white',
    password: 'SF_WHITE',
    waitForConnections: true,
    connectionLimit: 10,
    rowsAsArray: false,
    multipleStatements: true
}

D.tunnelConfig = {
    host: '159.89.208.60',
    port: 22,
    username: 'root',
    password: 'S3ns0rP@$$'
}

D.forwardConfig = {
    srcHost: 'localhost',
    srcPort: 3306,
    dstHost: D.dbServer.host,
    dstPort: D.dbServer.port
};


module.exports = D;