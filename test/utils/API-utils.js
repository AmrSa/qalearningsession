import request from 'supertest'

const baseUrl = 'http://white.sf.bluesky.sh/spaceframe/'
const req = request(baseUrl)

// let loginCookie;

class API {

    getLoginCookies(username, password) {
        return browser.call(async () => {
            return req
                .post('login/validate.json')
                .send({
                    username: username,
                    password: password
                })
                .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
                .expect(200)
                .then(res => {
                    const cookie = JSON.stringify(res.header['set-cookie']);
                    const PHPSESSID = (cookie.split(';')[0]).replace('["','');
                    const loginCookie = PHPSESSID.split('=')[1];
                    return loginCookie
                });
        });
    }

}

export default new API();