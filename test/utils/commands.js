import dashBoardPage from '../pages/dashBoardPage'
import DB from '../utils/DB-utils'

const c = exports

c.commands = function() {
    browser.addCommand('loginWith', (username,password)=>{
        dashBoardPage.loginWith(username,password)
    })

    browser.addCommand('submitDBQuery', (query)=>{
        return DB.submitDBQuery(query)
    })
}